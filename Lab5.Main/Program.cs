﻿using System;
using System.Windows;

using PK.Container;
using Lab5.Infrastructure;

using Lab5.DisplayForm;
using MainComponent.Contract;


namespace Lab5.Main
{
    public class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            /* Uruchomienie wątku dla aplikacji okienkowej */
            ApplicationStart();

            // Należy zaimplementować tę metodę
            IContainer container = Configuration.ConfigureApp();

            /*** Początek własnego kodu ***/

            var mikrofala = container.Resolve<IMagnetron>();
            Console.WriteLine("<<<<< Mikrofalówka >>>>>");
            Console.WriteLine();
            Console.WriteLine("1 - Włącz mikrofalę");
            Console.WriteLine("2 - Wyłącz mikrofalę");
            Console.WriteLine();
            Console.WriteLine("Proszę podać wartość:");
            string wartoscWyboru = Console.ReadLine();
            int outNumber;
            if (int.TryParse(wartoscWyboru, out outNumber))
            {
                if (wartoscWyboru == "1")
                {
                    mikrofala.EnergiaStart();
                    System.Threading.Thread.Sleep(3000);
                    MessageBox.Show("Włączono mikrofalę.", "Information", MessageBoxButton.OK, MessageBoxImage.Information);

                }
                else if (wartoscWyboru == "2")
                {
                    mikrofala.EnergiaStop();
                    System.Threading.Thread.Sleep(3000);
                    MessageBox.Show("Wyłączono mikrofalę.", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("Niepoprawny numer operacji. RESET!!!", "Failed", MessageBoxButton.OK, MessageBoxImage.Warning );
                }
            }
            else
            {
                MessageBox.Show("Błąd! ERROR! Wybrałeś niepoprawną opcję. Mikrofala zostanie wyłączona.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            //  Console.ReadKey();

            /*** Koniec własnego kodu ***/

            /* Zatrzymanie wątku dla aplikacji okienkowej */
            ApplicationStop();
        }

        private static void DisplayFormExample()
        {
            /* Uruchomienie kodu w wątku aplikacji okienkowej */
            var model = (DisplayViewModel)Application.Current.Dispatcher.Invoke(new Func<DisplayViewModel>(() =>
                {
                    // utworzenie nowej formatki graficznej stanowiącej widok
                    var form = new Form();
                    // utworzenie modelu widoku (wzorzec MVVM)
                    var viewModel = new DisplayViewModel();
                    // przypisanie modelu do widoku
                    form.DataContext = viewModel;
                    // wyświetlenie widoku
                    form.Show();
                    // zwrócenie modelu widoku do dalszych manipulacji
                    return viewModel;
                }), null);

            /* Modyfikacja właściwości modelu - zmiana napisu na wyświetlaczu */
            model.Text = "Test";
        }

        public static void ApplicationStart()
        {
            var thread = new System.Threading.Thread(CreateApp);
            thread.SetApartmentState(System.Threading.ApartmentState.STA);
            thread.Start();
            System.Threading.Thread.Sleep(300);
        }

        private static void CreateApp()
        {
            var app = new Application();
            app.ShutdownMode = ShutdownMode.OnExplicitShutdown;
            app.Run();
        }

        public static void ApplicationStop()
        {
            Application.Current.Dispatcher.InvokeShutdown();
        }

    }
}

