﻿using MainComponent.Contract;
using MainComponent.Implementation;
using System;
using System.Reflection;
using UserInterface.Contract;

namespace Lab5.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Type Container = typeof(MyOwnContainer.Container);

        #endregion

        #region P2

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(IMagnetron));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(Magnetron));

        public static Assembly DisplayComponentSpec = Assembly.GetAssembly(typeof(IUserInterface));
        public static Assembly DisplayComponentImpl = Assembly.GetAssembly(typeof(UserInterface.Implementation.UserInterface));

        #endregion
    }
}
