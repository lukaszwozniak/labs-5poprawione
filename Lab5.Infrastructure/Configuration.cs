﻿using System;
using PK.Container;
using System.Reflection;
using MyOwnContainer;

namespace Lab5.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        public static IContainer ConfigureApp()
        {
            Container container = new Container();
            container.Register(Assembly.Load("MainComponent.Implementation"));
            container.Register(Assembly.Load("MainComponent.Contract"));
            container.Register(Assembly.Load("UserInterface.Implementation"));
            container.Register(Assembly.Load("UserInterface.Contract"));
            return container;
        }
    }
}
