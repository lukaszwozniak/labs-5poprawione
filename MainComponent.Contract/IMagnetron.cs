﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainComponent.Contract
{
    public interface IMagnetron
    {
        void EnergiaStart();
        void EnergiaStop();
    }
}
