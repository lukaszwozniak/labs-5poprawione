﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserInterface.Contract
{
    public interface IUserInterface
    {
        void StopPrzesylaniaEnergii();
        void ZwrocInformacjeOStanie(string informacjaOStanie);
    }
}
