﻿using Lab5.DisplayForm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using UserInterface.Contract;

namespace UserInterface.Implementation
{
    public class UserInterface : IUserInterface
    {
        DisplayViewModel model;

        public UserInterface()
        {
            model = (DisplayViewModel)Application.Current.Dispatcher.Invoke(new Func<DisplayViewModel>(() =>
            {
                // utworzenie nowej formatki graficznej stanowiącej widok
                var form = new Form();
                // utworzenie modelu widoku (wzorzec MVVM)
                var viewModel = new DisplayViewModel();
                // przypisanie modelu do widoku
                form.DataContext = viewModel;
                // wyświetlenie widoku
                form.Show();
                // zwrócenie modelu widoku do dalszych manipulacji
                return viewModel;
            }), null);
        }

        void IUserInterface.ZwrocInformacjeOStanie(string informacja)
        {
            model.Text = informacja;
        }

        void IUserInterface.StopPrzesylaniaEnergii()
        {
            model.Text = "Sygnał Przesyłania Energii STOP";
        }
    }
}
