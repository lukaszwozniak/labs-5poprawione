﻿using MainComponent.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserInterface.Contract;

namespace MainComponent.Implementation
{
    public class Magnetron : IMagnetron
    {
        IUserInterface mainUserInterface;

        public Magnetron(IUserInterface userInterface)
        {
            mainUserInterface = userInterface;
        }

        void IMagnetron.EnergiaStart()
        {
            mainUserInterface.ZwrocInformacjeOStanie("Energia Start!");
        }

        void IMagnetron.EnergiaStop()
        {
            mainUserInterface.ZwrocInformacjeOStanie("Energia Stop!");
        }
    }
}
